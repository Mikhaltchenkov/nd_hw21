let http = require('http');
let url = require('url');
let querystring = require('querystring');

const port = 3000;

let server = http.createServer();
server.listen(port);

server.on('request', (req, res) => {
    let queryData = url.parse(req.url, true).query;
    if (queryData.firstname && queryData.lastname) {
        var post_options = {
            host: 'netology.tomilomark.ru',
            port: '80',
            path: '/api/v1/hash',
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'firstname': queryData.firstname
            }
        };

        let data = JSON.stringify({
                'lastname': queryData.lastname
            });

        let post_req = http.request(post_options, (result) =>{
            let data = '';
            result.setEncoding('utf8');
            result.on('data', chunk => data += chunk);
            result.on('end', () => {
                try {
                    data = JSON.parse(data);
                    let answer = JSON.stringify({
                        'firstName': queryData.firstname,
                        'lastName': queryData.lastname,
                        'secretKey': data.hash
                    });
                    res.writeHead(200, {"Content-Type": "application/json"});
                    res.write(answer);
                    res.end();
                } catch (err) {
                    res.writeHead(500, {"Content-Type": "application/json"});
                    let answer = JSON.stringify({
                        'message':err.message
                    });
                    res.write(answer);
                    res.end();
                }
            });
        });
        post_req.write(data);
        post_req.end();
        post_req.on('error',(err) => {
            res.writeHead(500, {"Content-Type": "application/json"});
            let answer = JSON.stringify({
                'message':err
            });
            res.write(answer);
            res.end();
        });
    } else {
        res.writeHead(400, {"Content-Type": "application/json"});
        let answer = JSON.stringify({
            'message':'Not specified Firstname and Lastname!'
        });
        res.write(answer);
        res.end();
    }
});

console.log('Browse to http://127.0.0.1:' + port);